import pandas as pd
import random
import json
import pika

haikus = json.load(open('./data/final-haikus.json', 'r'))
# df = pd.read_csv('./data/rarity.csv')
df = pd.read_csv('./data/SHIB-Rarity-Rubric.csv')
df['Cumulative'] = 0
attributes = df['Attribute'].unique()


for attribute in attributes:
    d = df[df['Attribute'] == attribute]
    assert d['Probability'].sum() == 1, 'Improper probability distribution'
    last_prob = 0
    for row in d.iterrows():
        prob = row[1]['Probability']
        df.loc[row[0], 'Cumulative'] = last_prob + prob
        last_prob += prob

template = {
    "name": "Shiba Inu Voter ID ",
    "description": "This Voter ID entitles the holder to vote on issues pertaining to the ethtrader subreddit",
    "image_url": "ipfs://",
    "animation_url": "ipfs://",
    "image": "ipfs://",
    "external_link": "https://reddit.com/r/ethtrader",
    "background_color": "ffffff",
    "owner": "0xa874Fa6ccDcCB57d9397247e088575C4EF34EC66",
    "attributes": [
        {
            "trait_type": "Style",
            "value": "Orange Texture"
        }
    ]
}


def generate_attributes(df):
    attributes = [{
        'trait_type': 'Haiku',
        'value': random.choice(haikus[0]) + '\n' + random.choice(haikus[1]) + '\n' + random.choice(haikus[2])
    }]
    for attribute in df['Attribute'].unique():
        d = df[df['Attribute'] == attribute]
        r = random.random()
        value = next(row[1]['Value'] for row in d.iterrows() if row[1]['Cumulative'] > r)
        attributes.append({
            "trait_type": attribute,
            "value": value
        })
    return attributes


def generate_configs(df, num_configs):
    configs = []
    for i in range(num_configs):
        if not i % 1000:
            print('Generating configs: {}'.format(i))
        configs.append({
            'name': 'Shiba Inu Voter ID {}'.format(i + 1),
            'description': 'This Voter ID entitles the holder to vote on issues pertaining to the SHIB token and the crypto space at large',
            'external_link': 'https://influencedotvote.eth.link',
            "background_color": "ffffff",
            "image_url": "ipfs://",
            "animation_url": "ipfs://",
            "image": "ipfs://",
            "owner": "0xa874Fa6ccDcCB57d9397247e088575C4EF34EC66",
            'attributes': generate_attributes(df)
        })
    return configs

configs = generate_configs(df, 1000)
print(df)

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('localhost', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
# connection = pika.BlockingConnection(pika.ConnectionParameters('localhost:5672'))

channel = connection.channel()
channel.queue_declare(queue='metadata', durable=True)

for i in range(len(configs)):
    config = configs[i]
    if not i % 1000:
        print('Publishing configs: {}'.format(i))
    config_str = json.dumps(config)
    channel.basic_publish(exchange='', routing_key='metadata', body=config_str, properties=pika.BasicProperties(delivery_mode=2))
