var ffmpeg = require('fluent-ffmpeg')

// ffmpeg -r 60 -s 1500x1500 -i ./screenshot-%03d.jpg -i audio2.mp3 -vcodec libx264 -c:a copy -crf 15  -pix_fmt yuv420p test.mp4

function write_mp4(images_path) {
  ffmpeg(images_path + '/screenshot-%03d.jpg')
    .inputOptions([
      '-r 60',
      '-i ./data/shibkebab.wav',
      '-pix_fmt yuv420p'
    ])
    .videoCodec('libx264')
    .audioCodec('libmp3lame')
    .size('1500x1500')
    .output(images_path + '/test.mp4')
    .on('end', () => { console.log('Finished processing') })
    .run()
}

write_mp4('./data/images')
